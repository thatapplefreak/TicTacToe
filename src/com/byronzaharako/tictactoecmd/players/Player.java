package com.byronzaharako.tictactoecmd.players;

import com.byronzaharako.tictactoecmd.TicTacToeException;
import com.byronzaharako.tictactoecmd.TicTacToeGrid;

import java.awt.Point;

/**
 * Base for creating a player
 * 
 * @author Byron Zaharako
 */
public abstract class Player {
    
    /**
     * enum containing basic player kinds
     */
    public enum PlayerKind {
        X,
        O
    }
    
    /**
     * The kind of player
     */
    protected final PlayerKind playerKind;
    
    /**
     * Type of player (playerName)
     */
    protected final String playerType;
    
    /**
     * Constructs a player that is a computer or human
     * 
     * @param playerKind Human or Computer
     */
    public Player(PlayerKind playerKind, String playerType) {
        this.playerKind = playerKind;
        this.playerType = playerType;
    }

    public char getLetter() {
        return this.playerKind.name().charAt(0);
    }
    
    /**
     * Processes the players move
     * 
     * @param grid the grid playing on
     * 
     * @return True if the player wishes to continue the game
     */
    public final boolean process(TicTacToeGrid grid) {
        System.out.println(playerType + " player " + playerKind.name() + " moving...");
        while (true) {
            try {
                Point move = move(grid.copy());
                if (move == null) {
                    continue;
                }
                if (move.getX() != -1 && move.getY() != -1) {
                    grid.setSpace(getLetter(), move);
                    System.out.println("Player puts " + playerKind + " at (" + (int)move.getX() + ", " + (int)move.getY() + ").");
                    return true;
                } else {
                    return false;
                }
            } catch (TicTacToeException e) {
                System.out.println(e.getMessage());
            }
        }
    }
    
    /**
     * Players move
     * 
     * @param grid the grid that the move is on
     * 
     * @return The point that the player chose
     */
    protected abstract Point move(TicTacToeGrid grid);
    
}

package com.byronzaharako.tictactoecmd.players;

import com.byronzaharako.tictactoecmd.TicTacToeGrid;

import java.awt.Point;

/**
 * Player that just goes from left to right
 * 
 * @author Byron Zaharako
 */
public class BadPlayer extends Player {

    public BadPlayer(PlayerKind playerKind) {
        super(playerKind, "bad");
    }

    /**
     * Player just moves on the next available space from left to right
     *
     * @param grid copy of the grid the player can see
     */
    @Override
    protected Point move(TicTacToeGrid grid) {
        for (int y = 0; y < grid.getSize() + 1; y++) {
            for (int x = 0; x < grid.getSize() + 1; x++) {
                Point p = new Point(x, y);
                if (grid.isOpen(p)) {
                    return p;
                }
            }
        }
        return null;
    }

}

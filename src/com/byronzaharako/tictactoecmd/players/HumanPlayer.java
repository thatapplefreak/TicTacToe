package com.byronzaharako.tictactoecmd.players;

import com.byronzaharako.tictactoecmd.TicTacToe;
import com.byronzaharako.tictactoecmd.TicTacToeGrid;

import java.awt.Point;

/**
 * Player that is an actual person on the computer
 * 
 * @author Byron Zaharako
 */
public class HumanPlayer extends Player {

    public HumanPlayer(PlayerKind playerKind) {
        super(playerKind, "human");
    }

    @Override
    protected Point move(TicTacToeGrid grid) {
        String input = null;
        try {
            Point returning = null;
            System.out
                    .print("Player " + playerKind.name() + ": Enter the col and row" +
                            "for a move (-1 to quit): ");
            input = TicTacToe.scanner.nextLine();
            if (input.contains("-1")) {
                returning = new Point(-1, -1);
            }
            String[] nums = input.split(" ");
            int column = Integer.parseInt(nums[0]);
            int row = Integer.parseInt(nums[1]);
            returning = new Point(column, row);
            return returning;
        } catch (NumberFormatException e) {
            System.out.println("Invalid input " + input);
        }
        return null;
    }

}

package com.byronzaharako.tictactoecmd.players;

import com.byronzaharako.tictactoecmd.TicTacToeException;
import com.byronzaharako.tictactoecmd.TicTacToeGrid;

import java.awt.Point;
import java.util.*;

/**
 * Player that Randomly chooses a space to fill
 * 
 * @author Byron Zaharako
 */
public class RandomPlayer extends Player {

    public RandomPlayer(PlayerKind playerKind) {
        super(playerKind, "random");
    }

    @Override
    protected Point move(TicTacToeGrid grid) {
        ArrayList<Point> legalMoves = new ArrayList<Point>();
        for (int x = 0; x < grid.getSize(); x++) {
            for (int y = 0; y < grid.getSize(); y++) {
                try {
                    if (grid.getSpace(x, y) == ' ') {
                        legalMoves.add(new Point(x, y));
                    }
                } catch (TicTacToeException e) {
                    System.out.println(e.getMessage());
                }
            }
        }        
        return legalMoves.get(new Random().nextInt(legalMoves.size()));
    }
}

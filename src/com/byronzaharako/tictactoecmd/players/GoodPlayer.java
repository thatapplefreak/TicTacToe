package com.byronzaharako.tictactoecmd.players;

import com.byronzaharako.tictactoecmd.TicTacToeException;
import com.byronzaharako.tictactoecmd.TicTacToeGrid;

import java.awt.Point;

/**
 * Player that plays the best possible move
 * 
 * @author Byron Zaharako
 */
public class GoodPlayer extends Player {

    public GoodPlayer(PlayerKind playerKind) {
        super(playerKind, "good");
    }

    @Override
    protected Point move(TicTacToeGrid grid) {
        /*
         * 1: check for empty grid
         * if the grid is empty (first move) place in the middle
         */
        if (grid.isEmpty()) {
            return new Point(grid.getSize() / 2, grid.getSize() / 2);
        }
        /*
         * 2: If this player can win this turn, take it
         * 
         */
        Point winCheck = checkForWin(grid);
        if (winCheck != null) {
            return winCheck;
        }
        /*
         * 3: If the other player has a win next turn, block it
         */
        Point blockCheck = checkForBlock(grid);
        if (blockCheck != null) {
            return blockCheck;
        }
        /*
         * 4: Execute strategy
         * 
         */
        return runStrategy(grid, false);
    }

    /**
     * Returns a point that if played will win the game
     * otherwise it returns null
     */
    private Point checkForWin(TicTacToeGrid grid) {
        char mySpace = this.playerKind.name().charAt(0);
        char emptySpace = ' ';
        //Check Rows
        for (int y = 0; y < grid.getSize(); y++) {
            int mySpaces = 0;
            int emptySpaces = 0;
            Point emptySpaceLoc = null;
            for (int x = 0; x < grid.getSize(); x++) {
                try {
                    if (grid.getSpace(x, y) == mySpace) {
                        mySpaces++;
                    } else if (grid.getSpace(x, y) == emptySpace) {
                        emptySpaceLoc = new Point(x, y);
                        emptySpaces++;
                    } else {
                        // Other players letter
                        break;
                    }
                } catch (TicTacToeException e) {
                    e.printStackTrace();
                }
            }
            if (mySpaces == (grid.getSize() - 1) && emptySpaces == 1) {
                return emptySpaceLoc;
            }
        }
        // Check Columns
        for (int x = 0; x < grid.getSize(); x++) {
            int mySpaces = 0;
            int emptySpaces = 0;
            Point emptySpaceLoc = null;
            for (int y = 0; y < grid.getSize(); y++) {
                try {
                    if (grid.getSpace(x, y) == mySpace) {
                        mySpaces++;
                    } else if (grid.getSpace(x, y) == emptySpace) {
                        emptySpaceLoc = new Point(x, y);
                        emptySpaces++;
                    } else {
                        // Other players letter
                        break;
                    }
                } catch (TicTacToeException e) {
                    e.printStackTrace();
                }
            }
            if (mySpaces == (grid.getSize() - 1) && emptySpaces == 1) {
                return emptySpaceLoc;
            }
        }
        // Check left diagonal
        {
            int mySpaces = 0;
            int emptySpaces = 0;
            Point emptySpaceLoc = null;
            for (int i = 0; i < grid.getSize(); i++) {
                try {
                    if (grid.getSpace(i, i) == mySpace) {
                        mySpaces++;
                    } else if (grid.getSpace(i, i) == emptySpace) {
                        emptySpaceLoc = new Point(i, i);
                        emptySpaces++;
                    } else {
                        break;
                    }
                } catch (TicTacToeException e) {
                    e.printStackTrace();
                }
            }
            if (mySpaces == (grid.getSize() - 1) && emptySpaces == 1) {
                return emptySpaceLoc;
            }
        }
        //Check right diagonal
        {
            int mySpaces = 0;
            int emptySpaces = 0;
            Point emptySpaceLoc = null;
            for (int i = 0; i < grid.getSize(); i++) {
                try {
                    if (grid.getSpace(i, grid.getSize() - 1 - i) == mySpace) {
                        mySpaces++;
                    } else if (grid.getSpace(i, grid.getSize() - 1 - i) == emptySpace) {
                        emptySpaceLoc = new Point(i, grid.getSize() - 1 - i);
                        emptySpaces++;
                    } else {
                        break;
                    }
                } catch (TicTacToeException e) {
                    e.printStackTrace();
                }
            }
            if (mySpaces == (grid.getSize() - 1) && emptySpaces == 1) {
                return emptySpaceLoc;
            }
        }
        // No possible wins this turn
        return null;
    }
    
    /**
     * Returns a point that if played will block the other player
     * if there are none it returns null
     */
    private Point checkForBlock(TicTacToeGrid grid) {
        char opponentSpace = this.playerKind.name().charAt(0) == 'X' ? 'O' : 'X';
        char emptySpace = ' ';
        //Check Rows
        for (int y = 0; y < grid.getSize(); y++) {
            int opponentSpaces = 0;
            int emptySpaces = 0;
            Point emptySpaceLoc = null;
            for (int x = 0; x < grid.getSize(); x++) {
                try {
                    if (grid.getSpace(x, y) == opponentSpace) {
                        opponentSpaces++;
                    } else if (grid.getSpace(x, y) == emptySpace) {
                        emptySpaceLoc = new Point(x, y);
                        emptySpaces++;
                    } else {
                        // my letter
                        break;
                    }
                } catch (TicTacToeException e) {
                    e.printStackTrace();
                }
            }
            if (opponentSpaces == (grid.getSize() - 1) && emptySpaces == 1) {
                return emptySpaceLoc;
            }
        }
        // Check Columns
        for (int x = 0; x < grid.getSize(); x++) {
            int opponentSpaces = 0;
            int emptySpaces = 0;
            Point emptySpaceLoc = null;
            for (int y = 0; y < grid.getSize(); y++) {
                try {
                    if (grid.getSpace(x, y) == opponentSpace) {
                        opponentSpaces++;
                    } else if (grid.getSpace(x, y) == emptySpace) {
                        emptySpaceLoc = new Point(x, y);
                        emptySpaces++;
                    } else {
                        // my letter
                        break;
                    }
                } catch (TicTacToeException e) {
                    e.printStackTrace();
                }
            }
            if (opponentSpaces == (grid.getSize() - 1) && emptySpaces == 1) {
                return emptySpaceLoc;
            }
        }
        // Check left diagonal
        {
            int opponentSpaces = 0;
            int emptySpaces = 0;
            Point emptySpaceLoc = null;
            for (int i = 0; i < grid.getSize(); i++) {
                try {
                    if (grid.getSpace(i, i) == opponentSpace) {
                        opponentSpaces++;
                    } else if (grid.getSpace(i, i) == emptySpace) {
                        emptySpaceLoc = new Point(i, i);
                        emptySpaces++;
                    } else {
                        break;
                    }
                } catch (TicTacToeException e) {
                    e.printStackTrace();
                }
            }
            if (opponentSpaces == (grid.getSize() - 1) && emptySpaces == 1) {
                return emptySpaceLoc;
            }
        }
        //Check right diagonal
        {
            int opponentSpaces = 0;
            int emptySpaces = 0;
            Point emptySpaceLoc = null;
            for (int i = 0; i < grid.getSize(); i++) {
                try {
                    if (grid.getSpace(i, grid.getSize() - 1 - i) == opponentSpace) {
                        opponentSpaces++;
                    } else if (grid.getSpace(i, grid.getSize() - 1 - i) == emptySpace) {
                        emptySpaceLoc = new Point(i, grid.getSize() - 1 - i);
                        emptySpaces++;
                    } else {
                        break;
                    }
                } catch (TicTacToeException e) {
                    e.printStackTrace();
                }
            }
            if (opponentSpaces == (grid.getSize() - 1) && emptySpaces == 1) {
                return emptySpaceLoc;
            }
        }
        // No possible blocks this turn
        return null;
    }
    
    /**
     * Modified algorithm of Bad that will only play into a row/column if another player does not occupy it
     * if these conditions cannot be met the players will remove the restriction
     */
    private Point runStrategy(TicTacToeGrid grid, boolean unrestrict) {
        char opponent = this.playerKind.name().charAt(0) == 'X' ? 'O' : 'X';
        //Rows
        for (int y = 0; y < grid.getSize(); y++) {
            boolean otherFound = false;
            for (int x = 0; x < grid.getSize(); x++) {
                try {
                    if (grid.getSpace(x, y) == opponent) {
                        otherFound = true;
                        break;
                    }
                } catch (TicTacToeException e) {
                    e.printStackTrace();
                }
            }
            if (unrestrict || !otherFound) {
                for (int i = 0; i < grid.getSize(); i++) {
                    try {
                        if (grid.getSpace(y, i) == ' ') {
                            Point move = new Point(y, i);
                            return move;
                        }
                    } catch (TicTacToeException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        //Columns
        for (int x = 0; x < grid.getSize(); x++) {
            boolean otherFound = false;
            for (int y = 0; y < grid.getSize(); y++) {
                try {
                    if (grid.getSpace(x, y) == opponent) {
                        otherFound = true;
                        break;
                    }
                } catch (TicTacToeException e) {
                    e.printStackTrace();
                }
            }
            if (unrestrict || !otherFound) {
                for (int i = 0; i < grid.getSize(); i++) {
                    try {
                        if (grid.getSpace(x, i) == ' ') {
                            Point move = new Point(x, i);
                            return move;
                        }
                    } catch (TicTacToeException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return runStrategy(grid, true);
    }

}

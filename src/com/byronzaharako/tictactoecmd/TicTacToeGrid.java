package com.byronzaharako.tictactoecmd;

import java.awt.Point;

/**
 * Grid containing the Xs and Os of the game
 * 
 * @author Byron Zaharako
 */
public class TicTacToeGrid {

    private char[][] spaces;
    
    private final int size;
    
    /**
     * Creates a ticktactoe grid of given size
     * 
     * @param size Size of grid to make
     */
    public TicTacToeGrid(int size) {
        this.size = size;
        //Adjust for 0 index
        spaces = new char[size][size];
        // Fill the spaces with " "
        for (int i = 0; i < spaces.length; i++) {
            for (int j = 0; j < spaces[i].length; j++) {
                spaces[i][j] = ' ';
            }
        }
    }
    
    /**
     * Get the size of the grid
     * 
     * @return the size of the grid
     */
    public int getSize() {
        return size;
    }
    
    /**
     * Set the space indicated
     * 
     * @param p the point to set
     * 
     * @throws TicTacToeException If the letter is not X or O,
     *                            the space was already filled,
     *                            or given an invalid location
     */
    public void setSpace(char letter, Point p) throws TicTacToeException {    
        if (!(letter == 'O' || letter == 'X')) {
            throw new TicTacToeException("invalid column and row: " + p.getX() + ", " + p.getY());
        }
        setSpace(letter, (int)p.getX(), (int)p.getY());
    }
    
    /**
     * Set the space indicated
     * 
     * @param x the column of the space
     * @param y the row of the space
     * 
     * @throws TicTacToeException If the letter is not X or O,
     *                            the space was already filled,
     *                            or given an invalid location
     */
    public void setSpace(char letter, int x, int y) throws TicTacToeException {    
        if (!(letter == 'O' || letter == 'X')) {
            throw new TicTacToeException(letter + " is not a valid move!");
        }
        try {
            if (spaces[x][y] == ' ') {
                spaces[x][y] = letter;
            } else {
                throw new TicTacToeException("invalid column and row: " + x
                        + ", " + y);
            }
        } catch (TicTacToeException e) {
            throw e;
        } catch (Exception e) {
            throw new TicTacToeException("invalid column and row: " + x
                    + ", " + y);
        }
    }
    
    /**
     * Gets the move at the space given
     * 
     * @param p The point to get from
     * @return The move at that location, null if empty
     */
    public char getSpace(Point p) throws TicTacToeException {
        return getSpace((int)p.getX(), (int)p.getY());
    }
    
    /**
     * Gets the move at the space given
     * 
     * @param x the column of the space
     * @param y the row of the space
     * 
     * @return The move at that location, " " if empty
     */
    public char getSpace(int x, int y) throws TicTacToeException {
        if ((x > (size - 1)) || (y > (size - 1))) {
            throw new TicTacToeException("invalid column and row: " + x + ", " + y);
        }
        return spaces[x][y];
    }
    
    /**
     * Returns true if the point on the grid is free
     * @param p the point
     * @return true if the point on the grid is free
     */
    public boolean isOpen(Point p) {
        try {
            return getSpace(p) == ' ';
        } catch (TicTacToeException e) {
            return false;
        }
    }
    
    /**
     * Prints the grid to the command prompt
     * example:
     * X|O|X
     * -+-+-
     *  |O| 
     * -+-+-
     *  |X|X
     */
    public void printGrid() {
        System.out.println();

        for (int y = 0; y < spaces.length; y++) {
            for (int x = 0; x < spaces.length; x++) {
                System.out.print(spaces[x][y]);
                if (x != spaces.length - 1) {
                    System.out.print('|');
                }
            }
            System.out.println();
            if (y != spaces.length - 1) {
                for (int i = 0; i < spaces.length; i++) {
                    if (i != spaces.length - 1) {
                        System.out.print("-+");
                    } else {
                        System.out.print('-');
                    }
                }
                System.out.println();
            }
        }
        
        System.out.println();
    }

    /**
     * Returns true if the grid has empty spaces
     * 
     * @return True if the grid has empty spaces
     */
    public boolean hasEmptySpaces() {
        for (int y = 0; y < spaces.length; y++) {
            for (int x = 0; x < spaces.length; x++) {
                if (spaces[x][y] == ' ') {
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * Returns true if empty
     * 
     * @return True if empty
     */
    public boolean isEmpty() {
        for (int i = 0; i < spaces.length; i++) {
            for (int j = 0; j < spaces[i].length; j++) {
                if (spaces[i][j] != ' ') {
                    return false;
                }
            }
        }
        return true;
    }

    public TicTacToeGrid copy() {
        TicTacToeGrid copy = new TicTacToeGrid(getSize());
        for (int y = 0; y < getSize(); y++) {
            for (int x = 0; x < getSize(); x++) {
                copy.spaces[x][y] = this.spaces[x][y];
            }
        }
        return copy;
    }
}

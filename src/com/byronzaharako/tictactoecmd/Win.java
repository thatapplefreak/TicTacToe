package com.byronzaharako.tictactoecmd;

/**
 * Win Types
 * 
 * @author Byron Zaharako
 */
public class Win {

    enum WinType {
        COLUMN,
        ROW,
        DIAGONAL;
    }
    
    /**
     * The type of win
     */
    private WinType type;
    
    /**
     * The row/column number for the win
     * this is never set if the win is diagonal
     */
    private int number;
    
    /**
     * The game winner
     */
    private char winner;
    
    /**
     * Constructs a win
     */
    Win(WinType type, char winner, int number) {
        this.type = type;
        this.winner = winner;
        if (type != WinType.DIAGONAL) {
            this.number = number;
        }
    }
    
    /**
     * Get the row/column number
     * @return the row/column number, null if diagonal
     */
    public int getNumber() {
        return number;
    }
    
    /**
     * Get the type of win
     * @return the type of win
     */
    public WinType getType() {
        return type;
    }
    
    /**
     * Returns the winner of the game
     * 
     * @return the winner of the game
     */
    public char getWinner() {
        return winner;
    }
    
}

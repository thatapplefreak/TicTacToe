package com.byronzaharako.tictactoecmd;

/**
 * Exception thrown by TicTacToe elements
 * 
 * @author Byron Zaharako
 */
public class TicTacToeException extends Exception {

    /**
     * Serial #
     */
    private static final long serialVersionUID = 8467672779952186294L;

    /**
     * Constructs a TicTacToeException
     * 
     * @param msg What went wrong
     */
    public TicTacToeException(String msg) {
        super(msg);
    }
    
}

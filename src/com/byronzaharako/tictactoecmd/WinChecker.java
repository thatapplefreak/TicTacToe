package com.byronzaharako.tictactoecmd;

import java.util.ArrayList;

/**
 * Helper class that checks for a Win case
 * 
 * @author Byron Zaharako
 */
public class WinChecker {

    /**
     * Checks the givin grid for a win
     * 
     * @param grid the grid to check
     * @return a Win case, null if none
     */
    public static Win checkGrid(TicTacToeGrid grid) {
        // Search rows
        for (int y = 0; y < grid.getSize(); y++) {
            ArrayList<Character> row = new ArrayList<Character>();
            for (int x = 0; x < grid.getSize(); x++) {
                try {
                    row.add(grid.getSpace(x, y));
                } catch (TicTacToeException e) {
                    e.printStackTrace();
                }
            }
            if (checkAll(row)) {
                return new Win(Win.WinType.ROW, row.get(0), y);
            }
        }
        // Search columns
        for (int x = 0; x < grid.getSize(); x++) {
            ArrayList<Character> column = new ArrayList<Character>();
            for (int y = 0; y < grid.getSize(); y++) {
                try {
                    column.add(grid.getSpace(x, y));
                } catch (TicTacToeException e) {
                    e.printStackTrace();
                }
            }
            if (checkAll(column)) {
                return new Win(Win.WinType.COLUMN, column.get(0), x);
            }
        }
        // Search diagonal left
        {
            ArrayList<Character> diag = new ArrayList<Character>();
            for (int i = 0; i < grid.getSize(); i++) {
                try {
                    diag.add(grid.getSpace(i, i));
                } catch (TicTacToeException e) {
                    e.printStackTrace();
                }
            }
            if (checkAll(diag)) {
                return new Win(Win.WinType.DIAGONAL, diag.get(0), 0);
            }
        }
        // Search diagonal right
        {
            ArrayList<Character> diag = new ArrayList<Character>();
            for (int i = 0; i < grid.getSize(); i++) {
                try {
                    diag.add(grid.getSpace(i, grid.getSize() - 1 - i));
                } catch (TicTacToeException e) {
                    e.printStackTrace();
                }
            }
            if (checkAll(diag)) {
                return new Win(Win.WinType.DIAGONAL, diag.get(0), 0);
            }
        }
        // No win cases
        return null;
    }
    
    /**
     * Checks given list to see if all characters are the same
     * 
     * @param list List of characters to check
     * @return true if all characters in the array are the same
     */
    private static boolean checkAll(ArrayList<Character> list) {
        for(int i = 0; i < list.size(); i++) {
            Character candidate = list.get(i);
            if(candidate == Character.valueOf(' ') || !candidate.equals(list.get(0))) {
                return false;
            }
        }
        return true;
    }
    
}

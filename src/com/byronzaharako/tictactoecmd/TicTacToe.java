package com.byronzaharako.tictactoecmd;

import com.byronzaharako.tictactoecmd.players.*;

import java.util.*;

/**
 * Main handler for TicTacToe
 * 
 * @author Byron Zaharako
 */
public class TicTacToe {
    
    // Runner Stuff ////////////////////////////////////////////
    
    /**
     * One scanner for the entire program
     */
    public static final Scanner scanner = new Scanner(System.in);
    
    /**
     * The exception thrown when the usage is incorrect
     */
    private static TicTacToeException usageException =
            new TicTacToeException("Usage: java TicTacToe player-X player-O [size]\n" +
                                   "where player-X and player-O are one of:\n" +
                                   "\thuman bad good random\n" +
                                   "and [size] is optional in the range from 1 to 3");

    public static void main(String[] args) {
        try {
            if (args.length < 2 || args.length > 3) {
                throw usageException;
            }
            String xPlayerType = args[0];
            String oPlayerType = args[1];
            int boardSize = 1;
            if (args.length == 3) {
                boardSize = Integer.parseInt(args[2]);
                if (boardSize < 1 || boardSize > 3) {
                    throw usageException;
                }
            }
            //Construct the X Player
            Player xPlayer = null;
            if (xPlayerType.equalsIgnoreCase("human")) {
                xPlayer = new HumanPlayer(Player.PlayerKind.X);
            } else if (xPlayerType.equalsIgnoreCase("bad")) {
                xPlayer = new BadPlayer(Player.PlayerKind.X);
            } else if (xPlayerType.equalsIgnoreCase("good")) {
                xPlayer = new GoodPlayer(Player.PlayerKind.X);
            } else if (xPlayerType.equalsIgnoreCase("random")) {
                xPlayer = new RandomPlayer(Player.PlayerKind.X);
            } else {
                throw usageException;
            }
            // Construct the O Player
            Player oPlayer = null;
            if (oPlayerType.equalsIgnoreCase("human")) {
                oPlayer = new HumanPlayer(Player.PlayerKind.O);
            } else if (oPlayerType.equalsIgnoreCase("bad")) {
                oPlayer = new BadPlayer(Player.PlayerKind.O);
            } else if (oPlayerType.equalsIgnoreCase("good")) {
                oPlayer = new GoodPlayer(Player.PlayerKind.O);
            } else if (oPlayerType.equalsIgnoreCase("random")) {
                oPlayer = new RandomPlayer(Player.PlayerKind.O);
            } else {
                throw usageException;
            }
            TicTacToe ttt = new TicTacToe(xPlayer, oPlayer, 3 + (boardSize * 2 - 2));
            ttt.play();
        } catch (TicTacToeException e) {
            System.err.println(e.getMessage());
        }
    }
    
    // Class Stuff ////////////////////////////////////////////////

    /**
     * Grid containing the moves of the players
     */
    private TicTacToeGrid theGrid;
    
    /**
     * Game players
     */
    private final Player xPlayer, oPlayer;
    
    /**
     * Construct a TicTacToe game
     * 
     * @param xPlayer Player that will be using Xs
     * @param oPlayer Player that will be using Os
     * @param boardSize length and width of the board
     */
    public TicTacToe(Player xPlayer, Player oPlayer, int boardSize) {
        theGrid = new TicTacToeGrid(boardSize);
        this.xPlayer = xPlayer;
        this.oPlayer = oPlayer;
    }
    
    /**
     * Begin the game, contains game loop
     */
    public void play() {
        int turn = 0;
        while (theGrid.hasEmptySpaces()) {
            theGrid.printGrid();
            Player currentPlayer = turn % 2 == 0 ? xPlayer : oPlayer;
            if (!currentPlayer.process(theGrid)) {
                System.out.println(
                        currentPlayer.getLetter()
                        + " quits the game");
                break;
            }
            // Win handle
            Win wincase = WinChecker.checkGrid(theGrid);
            if (wincase != null) {
                if (wincase.getType() == Win.WinType.DIAGONAL) {
                    System.out.println(wincase.getWinner() + " wins on the diagonal!");
                    break;
                } else {
                    System.out.println(
                            wincase.getWinner()
                            + " wins in "
                            + wincase.getType().name().toLowerCase()
                            + ": "
                            + wincase.getNumber());
                    break;
                }
            }
            // Tie handle
            if (!theGrid.hasEmptySpaces()) {
                System.out.println("Its a tie, no one wins.");
                break;
            }
            // Next turn
            turn++;
        }
        theGrid.printGrid();
    }
    
}
